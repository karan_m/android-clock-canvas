package com.gandan.clock;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.gandan.R;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by chandra on 4/18/15.
 */
public class ClockCanvas extends View {

    private Paint hourPaint, minutePaint, secondPaint;    // paint to draw line
    private RectF hourOval, minuteOval, secondOval; // draw arc inside oval
    private float hourStart, hourEnd;   // angle
    private float minuteStart, minuteEnd;   // angle
    private float secondStart, secondEnd;   // angle
    private final static boolean useCenter = false;
    private int diameter = 0;
    private int hourStroke = 20, minuteStroke = 10, secondStroke = 5;
    private int spaceBetweenArc = 5;
    private Calendar calendar;
    private Timer timer;
    private TimerTask timerTask;
    private Paint textPaint;
    private int hourColor, minuteColor, secondColor, textColor;
    private float textSize;

    public ClockCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ClockCanvas,
                0, 0);

        try {
            hourColor = a.getColor(R.styleable.ClockCanvas_hourColor, Color.RED);
            minuteColor = a.getColor(R.styleable.ClockCanvas_minuteColor, Color.BLUE);
            secondColor = a.getColor(R.styleable.ClockCanvas_secondColor, Color.GREEN);
            textColor = a.getColor(R.styleable.ClockCanvas_fontColor, Color.DKGRAY);
            textSize = a.getDimension(R.styleable.ClockCanvas_textSize, 48.f);
        } finally {
            a.recycle();
        }


        // initialize rectangular for drawing hour
        hourOval = new RectF(this.getLeft() + hourStroke, this.getTop() + hourStroke,
                this.getWidth(),
                this.getHeight());

        // setup paint for hour
        hourPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        hourPaint.setColor(hourColor);
        hourPaint.setStyle(Paint.Style.STROKE);
        hourPaint.setStrokeWidth(hourStroke);

        // initialize rectangular for drawing minute
        minuteOval = new RectF(hourOval.left + hourStroke + spaceBetweenArc,
                hourStroke + hourOval.top + spaceBetweenArc,
                hourOval.right - spaceBetweenArc,
                hourOval.bottom - spaceBetweenArc);

        // setup paint for minute
        minutePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        minutePaint.setColor(minuteColor);
        minutePaint.setStyle(Paint.Style.STROKE);
        minutePaint.setStrokeWidth(minuteStroke);


        // initialize rectangular for drawing second
        secondOval = new RectF(minuteOval.left + minuteStroke + spaceBetweenArc,
                minuteOval.top + minuteStroke + spaceBetweenArc,
                minuteOval.right - spaceBetweenArc,
                minuteOval.bottom - spaceBetweenArc);

        // setup paint for second
        secondPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        secondPaint.setColor(secondColor);
        secondPaint.setStyle(Paint.Style.STROKE);
        secondPaint.setStrokeWidth(minuteStroke);

        // initialize default value
        hourStart = 90; // angle 90
        minuteStart = 90;  // angle 90
        secondStart = 90; // angle 90
        hourEnd = 90;
        minuteEnd = 90;
        secondEnd = 90;

        // setup timer for updating time
        timer = new Timer();
        //handler = new Handler();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                postInvalidate();
                //Log.d("Timer", "called");
            }
        };

        timer.scheduleAtFixedRate(timerTask, 0, 1000);    // execute in 1 second interval

        // paint for clock text
        textPaint = new TextPaint();
        textPaint.setColor(textColor);
        textPaint.setTextSize(textSize);
        textPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        diameter = w < h ? w : h;

        // find diameter after calculate with hour stroke width
        diameter = diameter - hourStroke > 0 ? diameter - hourStroke : 0;
        hourOval.set(hourOval.left, hourOval.top, diameter, diameter);

        minuteOval.set(minuteOval.left,
                minuteOval.top,
                diameter - hourStroke - spaceBetweenArc,
                diameter - hourStroke - spaceBetweenArc);

        secondOval = new RectF(secondOval.left,
                secondOval.top,
                minuteOval.right - minuteStroke - spaceBetweenArc,
                minuteOval.bottom - minuteStroke - spaceBetweenArc);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // setup calendar instance
        calendar = Calendar.getInstance();


        // draw hour
        canvas.drawArc(hourOval, -hourStart, calendar.get(Calendar.HOUR) * 360 / 12, useCenter, hourPaint);

        // draw minute
        canvas.drawArc(minuteOval, -minuteStart, calendar.get(Calendar.MINUTE) * 360 / 60, useCenter, minutePaint);

        // draw second
        canvas.drawArc(secondOval, -secondStart, calendar.get(Calendar.SECOND) * 360 / 60, useCenter, secondPaint);

        // write text time
        String clock = "" + calendar.get(Calendar.HOUR) + " . " +
                calendar.get(Calendar.MINUTE) + " . " +
                calendar.get(Calendar.SECOND);
        canvas.drawText(clock,
                secondOval.left + (secondOval.right - secondOval.left - textPaint.getTextSize())/2,
                secondOval.top + (secondOval.bottom - secondOval.top - textPaint.measureText(clock))/2,
                textPaint);
    }

    public int getHourColor() {
        return hourColor;
    }

    public void setHourColor(int hourColor) {
        this.hourColor = hourColor;
        this.hourPaint.setColor(this.hourColor);
    }

    public int getMinuteColor() {
        return minuteColor;
    }

    public void setMinuteColor(int minuteColor) {
        this.minuteColor = minuteColor;
        this.minutePaint.setColor(this.minuteColor);
    }

    public int getSecondColor() {
        return secondColor;
    }

    public void setSecondColor(int secondColor) {
        this.secondColor = secondColor;
        this.secondPaint.setColor(this.secondColor);
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        this.textPaint.setColor(textColor);
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
        this.textPaint.setTextSize(this.textSize);
    }
}
